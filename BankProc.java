package model;
public interface BankProc {

/**
 * @pre person!=null
 * @post pairs.contains(person)
 */
public void addPerson(Person person);
/**
 * @pre person!=null
 * @post !person.contains(person) 
 */

public void removePerson(Person person);

/**
 * 
 * @pre person!=null
 * @pre acount!=null
 * @post pairs.get(person).contains(acount)
 */
public void addAcount(Person person, Acount acount);

/**
 * 
 * @pre person!=null
 * @pre acount!=null
 * @post !pairs.get(person).contains(acount)
 */
public void removeAcount(Person person, Acount acount);

	
	
	
}
