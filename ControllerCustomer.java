package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Acount;
import model.Bank;
import model.Person;
import model.SavingAcount;
import model.SpendingAcount;
import view.ViewCustomer;

public class ControllerCustomer {
	private ViewCustomer viewCustomer;
	private Bank bank;
	private static final String[] acountHeaders= {"Acount Number","Acount Owner","Hip","Period","Profit"};
	private static final String[] personHeaders= {"Id","Name","Age"};

	
	public ControllerCustomer(Bank bank) {
		this.bank=bank;
		viewCustomer=new ViewCustomer();
		viewCustomer.getMakeDepose().addActionListener(new ListenerMakeDeposeButton());
		viewCustomer.getMakeWithdraw().addActionListener(new ListenerMakeWithdrawButton());
		viewCustomer.getTableAcounts().addMouseListener(new ListenerTableAcounts());
		viewCustomer.getTablePersons().addMouseListener(new ListenerTablePersons());
		populateAcountsTable(viewCustomer.getTableAcounts(),getAcounts(),acountHeaders);
		populatePersonsTable(viewCustomer.getTablePersons(),bank.getPairs().keySet(), personHeaders);
	}
	
	public JTable populateAcountsTable(JTable table,ArrayList<Acount> acounts , String[] headers ) {
		DefaultTableModel defaultTableModel=new DefaultTableModel(new Object[][] {},headers);
		table.setModel(defaultTableModel);
		for (Acount acount : acounts) {
			ArrayList<String> strings=new ArrayList<>();
			strings.add(acount.getAcountNumber()+"");
			strings.add(acount.getAcountOwner()+"");
			strings.add(acount.getHip()+"");
			if(acount instanceof SavingAcount) {
				strings.add(((SavingAcount) acount).getPeriod()+"");
				strings.add(((SavingAcount) acount).getInterest()+"");
			}
			defaultTableModel.addRow(strings.toArray());
		}
		table.setBackground(Color.cyan);
		return table;
	}
	
	public JTable populatePersonsTable(JTable table,Set<Person> persons , String[] headers ) {
		DefaultTableModel defaultTableModel=new DefaultTableModel(new Object[][] {},headers);
		table.setModel(defaultTableModel);
		for (Person person : persons) {
			ArrayList<String> strings=new ArrayList<>();
			strings.add(person.getId()+"");
			strings.add(person.getName());
			strings.add(person.getAge()+"");
			defaultTableModel.addRow(strings.toArray());
		}
		table.setBackground(Color.cyan);
		return table;
	}
	
	public ArrayList<Acount> getAcounts(){
		ArrayList<Acount> list=new ArrayList<>();
		for(Person person:bank.getPairs().keySet())
			for(Acount acount:bank.getPairs().get(person))
				list.add(acount);
		return list;
	}
	
	public class ListenerMakeWithdrawButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			String name=viewCustomer.getPersonName().getText();
			int age=Integer.parseInt(viewCustomer.getPersonAge().getText());
			int id=Integer.parseInt(viewCustomer.getPersonId().getText());
			Person person=new Person(name, age, id);
			if(viewCustomer.getComboBox().getSelectedItem().equals("SavingAcount")) {
				double hip=Double.parseDouble(viewCustomer.getAcountHip().getText());
				int acountNumber=Integer.parseInt(viewCustomer.getAcountNumber().getText());
				String acountOwner=viewCustomer.getAcountOwner().getText();
				SavingAcount savingAcount=new SavingAcount(hip, acountNumber, acountOwner);
				for(Acount savingAcount2:bank.getPairs().get(person)) 
					if(savingAcount2 instanceof SavingAcount)
						if(savingAcount.equals((SavingAcount)savingAcount2))
							((SavingAcount)savingAcount2).withdraw(Integer.parseInt(viewCustomer.getInputSum().getText()));
				bank.serialize();
			}
			else {
				double hip=Double.parseDouble(viewCustomer.getAcountHip().getText());
				int acountNumber=Integer.parseInt(viewCustomer.getAcountNumber().getText());
				String acountOwner=viewCustomer.getAcountOwner().getText();
				SpendingAcount spendingAcount=new SpendingAcount(acountNumber, hip, acountOwner);
				for(Acount spendingAcount2: bank.getPairs().get(person))
					if(spendingAcount2 instanceof SpendingAcount)
						if(spendingAcount.equals((SpendingAcount)spendingAcount2))
							((SpendingAcount)spendingAcount2).withdraw(Double.parseDouble(viewCustomer.getInputSum().getText()));
				bank.serialize();
			}
			populateAcountsTable(viewCustomer.getTableAcounts(),getAcounts(),acountHeaders);
			
		}
		
	}
	
	public class ListenerMakeDeposeButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			String name=viewCustomer.getPersonName().getText();
			int age=Integer.parseInt(viewCustomer.getPersonAge().getText());
			int id=Integer.parseInt(viewCustomer.getPersonId().getText());
			Person person=new Person(name, age, id);
			if(viewCustomer.getComboBox().getSelectedItem().equals("SavingAcount")) {
				double hip=Double.parseDouble(viewCustomer.getAcountHip().getText());
				int acountNumber=Integer.parseInt(viewCustomer.getAcountNumber().getText());
				String acountOwner=viewCustomer.getAcountOwner().getText();
				SavingAcount savingAcount=new SavingAcount(hip, acountNumber, acountOwner);
				for(Acount savingAcount2:bank.getPairs().get(person)) 
					if(savingAcount2 instanceof SavingAcount)
						if(savingAcount.equals((SavingAcount)savingAcount2))
							((SavingAcount)savingAcount2).depose(Integer.parseInt(viewCustomer.getInputSum().getText()));
				bank.serialize();
			}
			else {
				double hip=Double.parseDouble(viewCustomer.getAcountHip().getText());
				int acountNumber=Integer.parseInt(viewCustomer.getAcountNumber().getText());
				String acountOwner=viewCustomer.getAcountOwner().getText();
				SpendingAcount spendingAcount=new SpendingAcount(acountNumber, hip, acountOwner);
				for(Acount spendingAcount2: bank.getPairs().get(person)) 
					if(spendingAcount2 instanceof SpendingAcount)
						if(spendingAcount.equals((SpendingAcount)spendingAcount2))
							((SpendingAcount)spendingAcount2).depose(Integer.parseInt(viewCustomer.getInputSum().getText()));
				bank.serialize();
			}
			populateAcountsTable(viewCustomer.getTableAcounts(),getAcounts(),acountHeaders);
			
		}
		
	}
	
	public class ListenerTableAcounts implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			int i=viewCustomer.getTableAcounts().getSelectedRow();
			viewCustomer.getAcountNumber().setText(viewCustomer.getTableAcounts().getModel().getValueAt(i, 0)+"");
			viewCustomer.getAcountOwner().setText(viewCustomer.getTableAcounts().getModel().getValueAt(i,1)+"");
			viewCustomer.getAcountHip().setText(viewCustomer.getTableAcounts().getModel().getValueAt(i,2)+"");			
		}

		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		
	}
	
	public class ListenerTablePersons implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			int i=viewCustomer.getTablePersons().getSelectedRow();
			viewCustomer.getPersonId().setText(viewCustomer.getTablePersons().getModel().getValueAt(i,0)+"");
			viewCustomer.getPersonName().setText(viewCustomer.getTablePersons().getModel().getValueAt(i,1)+"");
			viewCustomer.getPersonAge().setText(viewCustomer.getTablePersons().getModel().getValueAt(i,2)+"");
		}
		
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		
	}
}
