package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@SuppressWarnings("serial")
public class Bank implements BankProc, Serializable{
	private HashMap<Person,List<Acount>> pairs;
	
	public Bank() {
		pairs=new HashMap<>();
	}

	public void serialize() {
		try {
			ObjectOutputStream objectOutputStream=new ObjectOutputStream(new FileOutputStream("BankSerial.txt"));
			objectOutputStream.writeObject(pairs);
			objectOutputStream.close();
		} catch (IOException e) {
			System.out.println("Something went wrong");
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void deserialize() {
		try {
			FileInputStream fileInputStream=new FileInputStream("BankSerial.txt");
			ObjectInputStream objectInputStream=new ObjectInputStream(fileInputStream);
			pairs=(HashMap<Person, List<Acount>>)objectInputStream.readObject(); 
			objectInputStream.close();
			fileInputStream.close();
		} catch (IOException | ClassNotFoundException e) {
			System.out.println("Something went wrong!");
			e.printStackTrace();
		}
	}

	public void addPerson(Person person) {
		assert person!=null: "Null person not allowed!";
		assert isWeelFormed();
		pairs.put(person, new ArrayList<>());
		assert isWeelFormed():"Is not weel formed";
	}

	public void removePerson(Person person) {
		assert person!=null: "Null person not allowed";
		assert pairs.keySet().contains(person);
		assert isWeelFormed();
		pairs.remove(person);
	}

	public void addAcount(Person person, Acount acount) {
		assert person!=null:"Null person not allowed";
		assert acount!=null: "Null acount not allowed";
		assert isWeelFormed();
		assert !pairs.get(person).contains(acount):"Already have that acount";
		if(!(pairs.get(person).contains(acount))) {
			acount.addObserver(person);
			pairs.get(person).add(acount);
		}
		assert isWeelFormed();
	}

	public void removeAcount(Person person, Acount acount) {
		assert person!=null:"Null person not allowed";
		assert acount!=null: "Null acount not allowed";
		assert isWeelFormed();
		assert pairs.get(person).contains(acount):"This aount doesn't exist";
		pairs.get(person).remove(acount);
	}
 	
	public HashMap<Person, List<Acount>> getPairs() {
		return pairs;
	}
	
	public boolean isWeelFormed() {
		for (Person person : pairs.keySet()) {
			if(person.getAge()<0)
				return false;
			if(person.getName().equals(""))
				return false;
			for (Acount acount : pairs.get(person)) {
				if(acount.getHip()<0)
					return false;
				if(acount.getAcountOwner().equals(""))
					return false;
				if(acount.getAcountNumber()<0)
					return false;
				
			}
		}
		return true;
	}
}
