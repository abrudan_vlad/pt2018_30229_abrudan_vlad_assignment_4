package tests;


import junit.framework.TestCase;
import model.Acount;
import model.Bank;
import model.Person;
import model.SavingAcount;
import model.SpendingAcount;

public class Tests extends TestCase {
	private Bank bank=new Bank();
	private Person person=new Person("Vlad", 22, 10);
	private Person person2=new Person("Andrei", 40, 11);
	private Acount acount=new SavingAcount(3000, 1, 12, "Vlad");
	private Acount acount2=new SpendingAcount(2, 1000, "Vlad");
	
	public void setUp() {
		bank.addPerson(person);
		bank.addAcount(person, acount);
		bank.addAcount(person, acount2);
	}
	
	public void testAddPerson() {
		bank.addPerson(person2);
		assertEquals(true, bank.getPairs().containsKey(person2));
	}
	
	public void testRemovePerson() {
		bank.removePerson(person);
		assertEquals(false, bank.getPairs().containsKey(person));
	}
	
	public void testAddAcount() {
		bank.addAcount(person, new SpendingAcount(3, 2000, "Vlad"));
		assertEquals(true, bank.getPairs().get(person).contains(new SpendingAcount(3, 2000, "Vlad")));
	}
	
	public void testRemoveAcount() {
		bank.removeAcount(person, acount2);
		assertEquals(true, !bank.getPairs().get(person).contains(new SpendingAcount(2, 1000, "Vlad")));
	}
	
	public void tearDown() {
		bank=null;
		person=null;
		person2=null;
		acount=null;
		acount2=null;
	}

}
