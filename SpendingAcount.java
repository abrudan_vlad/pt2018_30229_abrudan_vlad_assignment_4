package model;
@SuppressWarnings("serial")
public class SpendingAcount extends Acount {

	public SpendingAcount() {
		super();
	}
	
	public SpendingAcount(int acountNumber,double hip, String acountOwner ) {
		super(acountNumber, hip, acountOwner);
	}
	
	public int withdraw(double sum) {
		if(sum<getHip()) {
			setChanged();
			setHip(getHip()-sum);
			notifyObservers(getHip());
			return 1;
		}
		else 
			return 0;
	}
	
	public int depose(int sum) {
		setChanged();
		setHip(getHip()+sum);
		notifyObservers(getHip());
		return 1;
	}
}
