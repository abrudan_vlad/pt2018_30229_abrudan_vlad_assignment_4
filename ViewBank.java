package view;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class ViewBank extends JFrame{
	private JButton addPerson=new JButton("Add Person");
	private JButton deletePerson=new JButton("Delete Person");
	private JButton editPerson=new JButton("Edit Person");
	private JButton addAcount=new JButton("Add Acount");
	private JButton deleteAcount=new JButton("Delete Acount");
	private JButton editAcount=new JButton("Edit Acount");
	private JTextArea personName=new JTextArea();
	private JTextArea personAge=new JTextArea();
	private JTextArea acountNumber=new JTextArea();
	private JTextArea acountOwner=new JTextArea();
	private JTextArea acountHip= new JTextArea();
	private JTextArea acountPeriod=new JTextArea();
	private JTextArea personId=new JTextArea();
	private JTable acountsTable=new JTable();
	private JTable personsTable=new JTable();
	private JComboBox<String> comboBox=new JComboBox<>();
	public ViewBank() {
		String[] options= {"SavingAcount","SpendingAcount"};
		comboBox.addItem(options[0]);
		comboBox.addItem(options[1]);
		JScrollPane scrollPane=new JScrollPane(acountsTable);
		JScrollPane scrollPane2=new JScrollPane(personsTable);
		JPanel panel= new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
		JPanel panel1=new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel1.add(createPane(personName, new JLabel("Person Name")));
		panel1.add(createPane(personAge, new JLabel("Person Age")));
		panel1.add(createPane(personId,new JLabel("Person ID")));
		JPanel panel2=new JPanel();
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
		panel2.add(createPane(acountNumber, new JLabel("Acount Number")));
		panel2.add(createPane(acountOwner, new JLabel("Acount Owner")));
		panel2.add(createPane(acountHip, new JLabel("Initial hip")));
		panel2.add(createPane(acountPeriod,new JLabel("Saving Period")));
		JPanel panel3=new JPanel();
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
		panel3.add(addAcount);
		panel3.add(editAcount);
		panel3.add(deleteAcount);
		panel3.add(comboBox);
		JPanel panel4=new JPanel();
		panel4.setLayout(new BoxLayout(panel4, BoxLayout.X_AXIS));
		panel4.add(addPerson);
		panel4.add(editPerson);
		panel4.add(deletePerson);
	
		panel.add(panel1);
		panel.add(panel4);
		panel.add(panel2);
		panel.add(panel3);
		panel.add(scrollPane);
		panel.add(scrollPane2);
		this.setContentPane(panel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setTitle("Bank manage page");
		this.setSize(new Dimension(600, 750));
		this.setLocation(750, 0);
	}
	
	public JPanel createPane(JTextArea textArea,JLabel label) {
		JPanel panel=new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(label);
		textArea.setMaximumSize(new Dimension(300,20));
		textArea.setMinimumSize(new Dimension(300, 20));
		panel.add(textArea);
		return panel;
	}
	
	public JButton getDeleteAcount() {
		return deleteAcount;
	}
	
	public JButton getDeletePerson() {
		return deletePerson;
	}
	
	public JButton getAddPerson() {
		return addPerson;
	}
	
	public JButton getAddAcount() {
		return addAcount;
	}
	
	public JButton getEditAcount() {
		return editAcount;
	}
	
	public JButton getEditPerson() {
		return editPerson;
	}
	
	public JTable getAcountsTable() {
		return acountsTable;
	}
	
	public JTable getPersonsTable() {
		return personsTable;
	}

	public JTextArea getPersonName() {
		return personName;
	}
	
	public JTextArea getPersonAge() {
		return personAge;
	}
	public JTextArea getPersonId() {
		return personId;
	}
	
	public JComboBox<String> getComboBox() {
		return comboBox;
	}
	
	public JTextArea getAcountHip() {
		return acountHip;
	}
	
	public JTextArea getAcountNumber() {
		return acountNumber;
	}
	
	public JTextArea getAcountOwner() {
		return acountOwner;
	}
	
	public JTextArea getAcountPeriod() {
		return acountPeriod;
	}

}
