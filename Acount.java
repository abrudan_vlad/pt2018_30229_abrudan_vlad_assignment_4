package model;

import java.io.Serializable;
import java.util.Observable;

@SuppressWarnings("serial")
public class Acount extends Observable implements Serializable{
	private int acountNumber;
	private double hip;
	private String acountOwner;
	
	public Acount() {
		acountNumber=0;
		hip=0;
		acountOwner="";
	}
	
	public Acount(int acountNumber, double hip, String acountOwner) {
		this.acountNumber = acountNumber;
		this.hip = hip;
		this.acountOwner = acountOwner;
	}

	
public int getAcountNumber() {
	return acountNumber;
}

public String getAcountOwner() {
	return acountOwner;
}

public double getHip() {
	return hip;
}

public void setAcountNumber(int acountNumber) {
	this.acountNumber = acountNumber;
}

public void setAcountOwner(String acountOwner) {
	this.acountOwner = acountOwner;
}

public void setHip(double hip) {
	this.hip = hip;
}

public String toString() {
	return acountOwner+","+acountNumber+","+hip;
}

@Override
public boolean equals(Object obj) {
	Acount acount=(Acount) obj;
	if(acount.getAcountNumber()==acountNumber)
		return true;
	else return false;
}


}
