package model;


@SuppressWarnings("serial")
public class SavingAcount extends Acount{
	private double  interest;
	private int period;
	private static final int minWithdraw=500;
	private static final int minDepose=500;
	private static final double interestRate=0.05;
	private int withdrawable=1;
	private int storable=1;
	
	public SavingAcount() {
		super();
	}
	
	public SavingAcount(double hip,int acountNumber,String acountOwner) {
		super(acountNumber, hip, acountOwner);
	}
	
	
	public SavingAcount(double hip, int acountNumber, int period, String acountOwner) {
		super(acountNumber, hip, acountOwner);
		this.period=period;
		this.interest=calculateInterest();
	}
	
	public double calculateInterest() {
		return interestRate*getHip()*period;
	}
	
	public int withdraw(int sum) {
		if(withdrawable==1)			
			if(sum<getHip())
				if(sum>minWithdraw) {
					setChanged();
					setHip(getHip()-sum);
					withdrawable--;
					interest=calculateInterest();
					notifyObservers(getHip());
					return 1;
				}
				else 
					return 0;
			else 
				return 0;
		else 
			return 0;
	}
	
	public int depose(int sum) {
		if(storable==1)
			if(sum>minDepose) {
				setChanged();
				setHip(getHip()+sum);
				storable--;
				interest=calculateInterest();
				notifyObservers(getHip());
				return 1;
			}
			else
				return 0;
		else 
			return 0;
	}
	
	public double getInterest() {
		return interest;
	}
	
	public int getPeriod() {
		return period;
	}
	
	public static int getMinwithdraw() {
		return minWithdraw;
	}
	
	
	public void setInterest(double interest) {
		this.interest = interest;
	}
	
	public void setPeriod(int period) {
		this.period = period;
	}
	
	public static int getMindepose() {
		return minDepose;
	}

	public int getWithdrawable() {
		return withdrawable;
	}

	public void setWithdrawable(int withdrawable) {
		this.withdrawable = withdrawable;
	}

	public int getStorable() {
		return storable;
	}

	public void setStorable(int storable) {
		this.storable = storable;
	}
	
}
