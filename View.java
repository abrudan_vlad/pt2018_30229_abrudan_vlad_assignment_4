package view;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class View extends JFrame{
private JButton personButton=new JButton("Customer");
private JButton acountButton=new JButton("Manage bank");
	public View() {
		JPanel panel=new JPanel();
		panel.add(personButton);
		panel.add(acountButton);
		this.setContentPane(panel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setTitle("Bank page");
		this.setSize(new Dimension(300, 100));
		this.setLocation(50, 50);
	}
	
	public JButton getPersonButton() {
		return personButton;
	}
	
	public JButton getAcountButton() {
		return acountButton;
	}
	
}
