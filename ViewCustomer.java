package view;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class ViewCustomer extends JFrame {
	private JButton makeWithdraw=new JButton("Make Withdraw");
	private JButton makeDepose=new JButton("Make Depose");
	private JTextArea inputSum=new JTextArea();
	private JTextArea acountNumber=new JTextArea();
	private JTextArea acountOwner=new JTextArea();
	private JTextArea acountHip= new JTextArea();
	private JTable tableAcounts=new JTable();
	private JTextArea personName= new JTextArea();
	private JTextArea personId=new JTextArea();
	private JTextArea personAge=new JTextArea();
	private JTable tablePersons=new JTable();
	private JComboBox<String> comboBox=new JComboBox<>();
	
	public ViewCustomer() {
		String[] options= {"SavingAcount","SpendingAcount"};
		comboBox.addItem(options[0]);
		comboBox.addItem(options[1]);
		JScrollPane scrollPane=new JScrollPane(tableAcounts);
		JPanel panel=new JPanel();
		JScrollPane scrollPane2=new JScrollPane(tablePersons);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JPanel panel4= new JPanel();
		panel4.setLayout(new BoxLayout(panel4, BoxLayout.X_AXIS));
		JPanel panel2=new JPanel();
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
		makeDepose.setAlignmentX(LEFT_ALIGNMENT);
		makeWithdraw.setAlignmentX(LEFT_ALIGNMENT);
		makeDepose.setMaximumSize(new Dimension(150, 30));
		makeWithdraw.setMaximumSize(new Dimension(150, 30));
		panel2.add(makeDepose);
		panel2.add(makeWithdraw);
		comboBox.setAlignmentX(LEFT_ALIGNMENT);
		comboBox.setMaximumSize(new Dimension(200, 30));
		panel2.add(comboBox);
		JPanel panel3 =new JPanel();
		panel3.setLayout(new BoxLayout(panel3,BoxLayout.Y_AXIS));
		panel3.add(createPane(inputSum, new JLabel("Sum"))); 
		panel3.add(createPane(acountHip, new JLabel("Current Hip")));
		panel3.add(createPane(acountNumber, new JLabel("Acount number")));
		panel3.add(createPane(acountOwner, new JLabel("Acount owner")));
		JPanel panel5= new JPanel();
		panel5.setLayout(new BoxLayout(panel5,BoxLayout.Y_AXIS));
		panel5.add(createPane(personId, new JLabel("Customer Id")));
		panel5.add(createPane(personName,new JLabel("Customer Name")));
		panel5.add(createPane(personAge, new JLabel("Customizer Age")));
		panel4.add(panel2);
		panel4.add(Box.createRigidArea(new Dimension(20, 0)));
		panel4.add(panel5);
		panel4.add(Box.createRigidArea(new Dimension(20, 0)));
		panel4.add(panel3);
		JPanel panel6=new JPanel();
		panel6.setLayout(new BoxLayout(panel6,BoxLayout.X_AXIS));
		panel6.add(scrollPane);
		panel6.add(scrollPane2);
		panel.add(panel4);
		panel.add(panel6);
		this.setContentPane(panel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setTitle("Acount manage page");
		this.setSize(new Dimension(900, 600));
		this.setLocation(50, 50);
		
	}
	public JPanel createPane(JTextArea textArea,JLabel label) {
		JPanel panel=new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		textArea.setAlignmentX(LEFT_ALIGNMENT);
		label.setAlignmentX(LEFT_ALIGNMENT);
		panel.add(label);
		textArea.setMaximumSize(new Dimension(200,20));
		textArea.setMinimumSize(new Dimension(200, 20));
		panel.add(textArea);
		return panel;
	}
	
	public JButton getMakeDepose() {
		return makeDepose;
	}
	
	public JButton getMakeWithdraw() {
		return makeWithdraw;
	}
	
	public JTable getTableAcounts() {
		return tableAcounts;
	}
	
	public JTextArea getInputSum() {
		return inputSum;
	}
	
	public JTextArea getAcountHip() {
		return acountHip;
	}
	
	public JTextArea getAcountNumber() {
		return acountNumber;
	}
	
	public JTextArea getAcountOwner() {
		return acountOwner;
	}
	
	public JTextArea getPersonAge() {
		return personAge;
	}
	
	public JTextArea getPersonId() {
		return personId;
	}
	
	public JTextArea getPersonName() {
		return personName;
	}
	
	public JTable getTablePersons() {
		return tablePersons;
	}
	
	public JComboBox<String> getComboBox() {
		return comboBox;
	}
	
}
