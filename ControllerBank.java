package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Acount;
import model.Bank;
import model.Person;
import model.SavingAcount;
import model.SpendingAcount;
import view.ViewBank;

public class ControllerBank {

	private ViewBank viewBank;
	private Bank bank;
	private static final String[] acountHeaders= {"Acount Number","Acount Owner","Hip","Period","Profit"};
	private static final String[] personHeaders= {"Id","Name","Age"};
	public ControllerBank(Bank bank) {
		this.bank=bank;
		viewBank=new ViewBank();
		viewBank.getAddAcount().addActionListener(new ListenerAddAcountButton());
		viewBank.getAddPerson().addActionListener(new ListenerAddPersonButton());
		viewBank.getDeleteAcount().addActionListener(new ListenerDeleteAcountButton());
		viewBank.getDeletePerson().addActionListener(new ListenerDeletePersonButton());
		viewBank.getEditAcount().addActionListener(new ListenerUpdateAcountButton());
		viewBank.getEditPerson().addActionListener(new ListenerUpdatePersonButton());
		viewBank.getAcountsTable().addMouseListener(new ListenerTableAcounts());
		viewBank.getPersonsTable().addMouseListener(new ListenerTablePersons());
		populateAcountsTable(viewBank.getAcountsTable(),getAcounts(),acountHeaders);
		populatePersonsTable(viewBank.getPersonsTable(),bank.getPairs().keySet(), personHeaders);
	}
	
	public JTable populateAcountsTable(JTable table,ArrayList<Acount> acounts , String[] headers ) {
		DefaultTableModel defaultTableModel=new DefaultTableModel(new Object[][] {},headers);
		table.setModel(defaultTableModel);
		for (Acount acount : acounts) {
			ArrayList<String> strings=new ArrayList<>();
			strings.add(acount.getAcountNumber()+"");
			strings.add(acount.getAcountOwner()+"");
			strings.add(acount.getHip()+"");
			if(acount instanceof SavingAcount) {
				strings.add(((SavingAcount) acount).getPeriod()+"");
				strings.add(((SavingAcount) acount).getInterest()+"");
			}
			defaultTableModel.addRow(strings.toArray());
		}
		table.setBackground(Color.cyan);
		return table;
	}
	
	public JTable populatePersonsTable(JTable table,Set<Person> persons , String[] headers ) {
		DefaultTableModel defaultTableModel=new DefaultTableModel(new Object[][] {},headers);
		table.setModel(defaultTableModel);
		for (Person person : persons) {
			ArrayList<String> strings=new ArrayList<>();
			strings.add(person.getId()+"");
			strings.add(person.getName());
			strings.add(person.getAge()+"");
			defaultTableModel.addRow(strings.toArray());
		}
		table.setBackground(Color.cyan);
		return table;
	}
	
	public ArrayList<Acount> getAcounts(){
		ArrayList<Acount> list=new ArrayList<>();
		for(Person person:bank.getPairs().keySet())
			for(Acount acount:bank.getPairs().get(person))
				list.add(acount);
		return list;
	}
	
	public class ListenerAddPersonButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			String name=viewBank.getPersonName().getText();
			int age=Integer.parseInt(viewBank.getPersonAge().getText());
			int id=Integer.parseInt(viewBank.getPersonId().getText());
			Person person=new Person(name, age, id);
			bank.addPerson(person);
			bank.serialize();
			populatePersonsTable(viewBank.getPersonsTable(), bank.getPairs().keySet(),personHeaders );
			
		}
		
	}
	
	public class ListenerAddAcountButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			String name=viewBank.getPersonName().getText();
			int age=Integer.parseInt(viewBank.getPersonAge().getText());
			int id=Integer.parseInt(viewBank.getPersonId().getText());
			Person person=new Person(name, age, id);
			if(viewBank.getComboBox().getSelectedItem().equals("SavingAcount")) {
				int hip=Integer.parseInt(viewBank.getAcountHip().getText());
				int acountNumber=Integer.parseInt(viewBank.getAcountNumber().getText());
				int period=Integer.parseInt(viewBank.getAcountPeriod().getText());
				String acountOwner=viewBank.getAcountOwner().getText();
				SavingAcount savingAcount=new SavingAcount(hip, acountNumber, period, acountOwner);
				bank.addAcount(person,savingAcount);
				bank.serialize();
			}
			else {
				double hip=Double.parseDouble(viewBank.getAcountHip().getText());
				int acountNumber=Integer.parseInt(viewBank.getAcountNumber().getText());
				String acountOwner=viewBank.getAcountOwner().getText();
				Acount spendingAcount=new SpendingAcount(acountNumber, hip, acountOwner);
				bank.addAcount(person, spendingAcount);	
				bank.serialize();
			}
			populateAcountsTable(viewBank.getAcountsTable(),getAcounts(),acountHeaders);
		}
		
	}
	
	public class ListenerDeletePersonButton implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String name=viewBank.getPersonName().getText();
			int age=Integer.parseInt(viewBank.getPersonAge().getText());
			int id=Integer.parseInt(viewBank.getPersonId().getText());
			Person person=new Person(name, age, id);
			bank.removePerson(person);
			bank.serialize();
			populatePersonsTable(viewBank.getPersonsTable(), bank.getPairs().keySet(),personHeaders );
			populateAcountsTable(viewBank.getAcountsTable(),getAcounts(),acountHeaders);
		}
		
	}
	
	public class ListenerDeleteAcountButton implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String name=viewBank.getPersonName().getText();
			int age=Integer.parseInt(viewBank.getPersonAge().getText());
			int id=Integer.parseInt(viewBank.getPersonId().getText());
			Person person=new Person(name, age, id);
			if(viewBank.getComboBox().getSelectedItem().equals("SavingAcount")) {
				int hip=Integer.parseInt(viewBank.getAcountHip().getText());
				int acountNumber=Integer.parseInt(viewBank.getAcountNumber().getText());
				int period=Integer.parseInt(viewBank.getAcountPeriod().getText());
				String acountOwner=viewBank.getAcountOwner().getText();
				SavingAcount savingAcount=new SavingAcount(hip, acountNumber, period, acountOwner);
				bank.removeAcount(person,savingAcount);
				bank.serialize();
			}
			else {
				double hip=Double.parseDouble(viewBank.getAcountHip().getText());
				int acountNumber=Integer.parseInt(viewBank.getAcountNumber().getText());
				String acountOwner=viewBank.getAcountOwner().getText();
				Acount spendingAcount=new SpendingAcount(acountNumber, hip, acountOwner);
				bank.removeAcount(person, spendingAcount);
				bank.serialize();
			}
			populateAcountsTable(viewBank.getAcountsTable(),getAcounts(),acountHeaders);
		}
		
	}
	
	public class ListenerUpdatePersonButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public class ListenerUpdateAcountButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public class ListenerTableAcounts implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			int i=viewBank.getAcountsTable().getSelectedRow();
			viewBank.getAcountNumber().setText(viewBank.getAcountsTable().getModel().getValueAt(i, 0)+"");
			viewBank.getAcountOwner().setText(viewBank.getAcountsTable().getModel().getValueAt(i,1)+"");
			viewBank.getAcountHip().setText(viewBank.getAcountsTable().getModel().getValueAt(i,2)+"");
			viewBank.getAcountPeriod().setText(viewBank.getAcountsTable().getModel().getValueAt(i, 3)+"");
		}

		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}

	}
	
	public class ListenerTablePersons implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			int i=viewBank.getPersonsTable().getSelectedRow();
			viewBank.getPersonId().setText(viewBank.getPersonsTable().getModel().getValueAt(i,0)+"");
			viewBank.getPersonName().setText(viewBank.getPersonsTable().getModel().getValueAt(i,1)+"");
			viewBank.getPersonAge().setText(viewBank.getPersonsTable().getModel().getValueAt(i,2)+"");
		}

		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		
	}
}
