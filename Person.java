package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class Person implements Observer,Serializable{
	private String name;
	private int age;
	private int id;
	
	
	
	public Person(String name, int age, int id) {
		this.age=age;
		this.name=name;
		this.id=id;
	}
	
	public void update(Observable o, Object arg) {
		
		System.out.println("S-a modificat soldul!!");
		JOptionPane.showMessageDialog(null, "Noul sold al contului este:"+arg);
	}
	
	
	public int getAge() {
		return age;
	}
	
	public String getName() {
		return name;
	}
	
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return age+","+name;
	}
	
	public boolean equals(Object person) {
		if(name.equals(((Person)person).getName()))
			if(age==((Person)person).getAge())
				if(id==((Person)person).getId())
					return true;
		return false;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
}
