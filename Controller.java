package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Bank;
import view.*;
public class Controller {
	private View view;
	private Bank bank;
	
	public Controller() {
		bank=new Bank();
		
		bank.deserialize();
		view=new View();
		view.getPersonButton().addActionListener(new ListenerPersonButton());
		view.getAcountButton().addActionListener(new ListenerAcountButton());
		
	}

	public class ListenerPersonButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			@SuppressWarnings("unused")
			ControllerCustomer controllerCustomer=new ControllerCustomer(getBank());
		}
		
	}
	
	public class ListenerAcountButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			@SuppressWarnings("unused")
			ControllerBank controllerBank=new ControllerBank(getBank());
		}
		
	}
	
	public Bank getBank() {
		return bank;
	}
}
